package com.example.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.dao.UserDao;
import com.example.model.User;


@Controller
//@RestController // Spring MVC
//@Component
//@ResponseBody
public class UserController {


	final static Logger logger = Logger.getLogger(UserController.class);
	
  /**
   * Create a new user with an auto-generated id and email and name as passed 
   * values.
   */
	
	@RequestMapping("/")
	public String welcome() {
	//	model.put("message", this.message);
		return "welcome";
	}
	
  @RequestMapping(value="/create")
  @ResponseBody
  public ResponseEntity<Void> create(@RequestBody User user) {
    try {
    //  User user1 = new User(user.getEmail(), user.getName());
      userDao.create(user);
    
    }
    catch (Exception ex) {
      return new ResponseEntity<Void>(HttpStatus.NOT_MODIFIED);//"Error creating the user: " + ex.toString();
    }
    return new ResponseEntity<Void>(HttpStatus.CREATED);
  }
  
  /**
   * Delete the user with the passed id.
   */
  @RequestMapping(value="/delete/{id}")
  @ResponseBody
  public String delete(@PathVariable("id") Long id) {
    try {
      User user = new User(id);
      userDao.delete(user);
    }
    catch (Exception ex) {
      return "Error deleting the user: " + ex.toString();
    }
    return "User succesfully deleted!";
  }
  
  /**
   * Retrieve the id for the user with the passed email address.
   */
  @RequestMapping(value="/get-by-email")
  @ResponseBody
  public String getByEmail(String email) {
    String userId;
    try {
      User user = userDao.getByEmail(email);
      userId = String.valueOf(user.getId());
    }
    catch (Exception ex) {
      return "User not found: " + ex.toString();
    }
    return "The user id is: " + userId;
  }
  
  /**
   * Update the email and the name for the user indentified by the passed id.
   */
  @RequestMapping(value="/update")
  @ResponseBody
  public String updateName(@RequestBody User user) {
    try {
     User user1 = userDao.getById(user.getId());
      user1.setEmail(user.getEmail());
      user1.setName(user.getName());
      userDao.update(user1);
    }
    catch (Exception ex) {
      return "Error updating the user: " + ex.toString();
    }
    return "User succesfully updated!";
  } 
/*
  @RequestMapping(value="/view", produces = {
	        MediaType.APPLICATION_JSON_VALUE},
	        method = RequestMethod.GET)*/
  @RequestMapping(value = "/view", method = RequestMethod.GET)
  @Consumes({ MediaType.APPLICATION_JSON})
  @Produces({ MediaType.APPLICATION_JSON})
  @ResponseBody
  public ResponseEntity<List<User>> view() {
	  List<User> userList;
    try {
    //  User user1 = new User(user.getEmail(), user.getName());
     userList = userDao.getAll();
    
     if (userList.isEmpty()) {
			logger.debug("Users does not exists");
			return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);
		}
		logger.debug("Found " + userList.size() + " Employees");
		logger.debug(userList);
		logger.debug(Arrays.toString(userList.toArray()));
    
    }
    catch (Exception ex) {
      return new ResponseEntity<List<User>>(HttpStatus.NOT_FOUND);//"Error creating the user: " + ex.toString();
    }
    return new ResponseEntity<List<User>>(userList,new HttpHeaders(), HttpStatus.OK);//HttpStatus.FOUND);
  }
  
  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<User> getUser(@PathVariable("id") Long id) {
		User user = userDao.getById(id);
		if (user == null) {
			logger.debug("User with id " + id + " does not exists");
			return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		}
		logger.debug("Found User:: " + user);
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}
  
  @RequestMapping(value = "/audit/{id}/{rev}", method = RequestMethod.GET)
 	public ResponseEntity<User> getAudit(@PathVariable("id") Long id,@PathVariable("rev") Integer rev) {
	  User user;
	    try {
	    //  User user1 = new User(user.getEmail(), user.getName());
	    	user = userDao.getAudit(id,rev);
	    
	    
	    }
	    catch (Exception ex) {
	      return new ResponseEntity<User>(HttpStatus.NOT_FOUND);//"Error creating the user: " + ex.toString();
	    }
	    return new ResponseEntity<User>(user,HttpStatus.FOUND);
 	}
  
  // Private fields
  
  // Wire the UserDao used inside this controller.
  @Autowired
  private UserDao userDao;
  
}