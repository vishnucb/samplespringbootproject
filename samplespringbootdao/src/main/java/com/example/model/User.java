package com.example.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

@Entity
@Table(name = "users")
@Audited
public class User implements Serializable{

  /**
	 * 
	 */
	private static final long serialVersionUID = -806975651514413359L;

// The entity fields (private)  
  
  @Id
  //@GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;
  
  @NotNull
  private String email;
  
  @NotNull
  private String name;


  // Public methods
  
  public User() { }

  public User(long id) { 
    this.id = id;
  }

  public User(String email, String name) {
    this.email = email;
    this.name = name;
  }

public long getId() {
	return id;
}

public void setId(long id) {
	this.id = id;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

 
  
}
